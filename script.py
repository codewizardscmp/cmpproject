import numpy as np
import matplotlib.pyplot as plt


my_data = np.loadtxt("data.d")

step = 1
plt.plot(my_data[:,0][::step], my_data[:,1][::step], 'r')
plt.plot(my_data[:,0][::step], my_data[:,2][::step], 'r')
plt.plot(my_data[:,0][::step], my_data[:,3][::step], 'b')
plt.plot(my_data[:,0][::step], my_data[:,4][::step], 'b')
plt.show()
