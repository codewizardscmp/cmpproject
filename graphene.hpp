#ifndef GRAPHENE_H
#define GRAPHENE_H

#include <iostream>
#include <vector>

typedef std::vector <double> vec;

class tight_binding
{

const int spatial_dimension = 2;

public:

vec bloch(spatial_dimension);

vec real_space_basis_vector(int spatial_dimension, double x_component, double y_component);

double dispersion_relation(bloch, )

};

#endif
