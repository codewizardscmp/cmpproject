CXX = g++
CXX_STD = -std=c++14 -O3 -Wall -Wextra #-Werror

GSL_CFLAGS = -I/usr/local/include/eigen3
GSL_LFLAGS =

CXX_CFLAGS = ${CXX_STD} ${GSL_CFLAGS}
CXX_LFLAGS = ${GSL_LFLAGS}
#============================================================

OBJS = external_fields.o

main : main.o ${OBJS}
	@echo "---- Linking $< -----"
	${CXX} -w ${OBJS} $< ${CXX_LFLAGS} -o $@.out
	-${RM} $<.o
	@echo "==============="

main.o : main.cpp
	@echo "--- Compiling $< ---"
	${CXX} -c $< ${CXX_CFLAGS} -o $@

external_fields.o : external_fields.cpp
	@echo "--- Compiling $< ---"
	${CXX} -c $< ${CXX_CFLAGS} -o $@

clean:
	rm *.out *.o *.dat *.d
