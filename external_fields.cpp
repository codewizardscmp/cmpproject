#include "external_fields.hpp"
#include <cmath>

double external_fields::gaussian_modulated_sinusoidal(double time, double witdh, double center_time, double frequency, double init_phase)
{

        return std::exp(-std::pow(time-center_time, 2.0)/(2.0 * std::pow(width, 2.0))) * std::cos(frequency * time + init_phase);

}
