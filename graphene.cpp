#include "graphene.hpp"
#include <cmath>

typedef std::vector <double> vec;

vec tight_binding::real_space_basis_vector(int spatial_dimension, double x_component, double y_component)
{
        vec basis_vector(spatial_dimension) = {
                x_component, y_component
        }
        return basis_vector
}
