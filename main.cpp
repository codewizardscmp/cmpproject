#include <iostream>
#include <cmath>
#include <complex>
#include <vector>
#include <fstream>
#include <Eigen/Eigen>
#include <unistd.h>
#include <chrono>
#include "external_fields.hpp"

using complex = std::complex<double>;
using namespace std::complex_literals;


complex delta(double a1_x, double a1_y, double a2_x, double a2_y, double k_x, double k_y)
{
        return std::exp(1i * (k_x * (a1_x + a2_x) + k_y * (a1_y + a2_y))/3.)* \
               (1.0 + std::exp(-1i * (k_x * a1_x + k_y * a1_y)) + std::exp(-1i * (k_x * a2_x + k_y * a2_y)));
}

double E_x(double E0_x, double sigma, double w, double phi_0, double t, double t_center)
{
        return E0_x * std::exp(-std::pow(t-t_center,2.)/std::pow(sigma, 2.)) * std::cos(w * t + phi_0);
}

double f_x(double E0_x, double sigma, double w, double phi_0, double t, double t_center)
{
        return -E_x(E0_x, sigma, w, phi_0, t, t_center);
}

double E_y(double E0_y, double sigma, double w, double phi_0, double t, double t_center)
{
        return E0_y * std::exp(-std::pow(t-t_center,2.)/std::pow(sigma, 2.)) * std::cos(w * t + phi_0);
}

double f_y(double E0_y, double sigma, double w, double phi_0, double t, double t_center)
{
        return -E_y(E0_y, sigma, w, phi_0, t, t_center);
}

complex f0(complex rho11, complex rho12, double a1_x, double a1_y, double a2_x, double a2_y, double kt_x, double kt_y, double damping_coeff)
{
        complex rho11_eq = 1.0/2.0;

        return -1i * (-delta(a1_x, a1_y, a2_x, a2_y, kt_x,  kt_y) * std::conj(rho12) + rho12 * std::conj(delta(a1_x, a1_y, a2_x, a2_y, kt_x, kt_y))) - damping_coeff * (rho11 - rho11_eq);
}

complex f1(complex rho11, complex rho12, double a1_x, double a1_y, double a2_x, double a2_y, double kt_x, double kt_y, \
           double damping_coeff, double thermal_energy, double fd_minus, double fd_plus)
{
        complex rho12_eq = 0.5 * (fd_minus - fd_plus) * delta(a1_x, a1_y, a2_x, a2_y, kt_x, kt_y)/(std::abs(delta(a1_x, a1_y, a2_x, a2_y, kt_x, kt_y)));

        return -1i * (-delta(a1_x, a1_y, a2_x, a2_y, kt_x, kt_y) * (1.0 - rho11) + rho11 * delta(a1_x, a1_y, a2_x, a2_y, kt_x, kt_y)) -  damping_coeff * (rho12 - rho12_eq);
}

int main()
{
        // primitive vectors of the real lattice
        double a1_x, a1_y;      // declaration of the (x,y) components of a primitive vector of the real lattice
        double a2_x, a2_y;      // declaration of the (x,y) components of a primitive vector of the real lattice

        a1_x = 3/2.; a1_y = std::sqrt(3)/2.; // initialization of the (x,y) components of a primitive vector of the real lattice
        a2_x = 0.0; a2_y = std::sqrt(3);  // initialization of the (x,y) components of a primitive vector of the real lattice

        // first Brillouin zone vector components

        double kt_x, kt_y; // declaration of the (x,y) components of the time dependent Brillouin zone vector

        double k_x, k_y; // declaration of the (x,y) components of the initial time Brillouin zone vector

        k_x = 0.9; k_y = 0.1;  // initialization of the (x,y) components of the initial time Brillouin zone vector

        double damping_coeff;    // declaration of the damping parameter or coefficient

        damping_coeff = 1.00;   // initialization of the damping parameter or coefficient

        double thermal_energy; // declaration of the thermal energy parameter

        thermal_energy = 0.1; // initialization of the thermal energy parameter

        // integration parameters of the Prince-Dormand (Runge-Kutta Class)
        double t;                                       // declaration of the time variable
        double initial_time, final_time;                                // declaration of the  initial and final time variables of integration, respectively.
        int N;                          // declaration of the number of uniform time integration iterations
        double initial_time_step;                             // declaration of the uniform time integration iteration variable
        int it;                                      // declaration of the iteration variable
        double min_time_step;
        double max_time_step;

        initial_time = 0.0;                                      // initialization of the initial time of integration variable
        final_time   = 100.0;                                    // initialization of the final time of integration variable
        N = 1000000;                    // initialization of the naive number of time integration iterations
        initial_time_step = (final_time- initial_time)/(N-1); // initialization of time integration iteration or step variable

        complex ck01, ck02, ck03, ck04, ck05, ck06, ck07;
        complex ck11, ck12, ck13, ck14, ck15, ck16, ck17;

        complex rho11_0, rho12_0, rho21_0, rho22_0; // declaration of the equilibrium reduced density matrix initial elements
        complex rho11, rho12, rho21, rho22;         // declaration of the time dependent reduced density matrix elements

        double e_plus, e_minus;

        e_plus  =  std::abs(delta(a1_x, a1_y, a2_x, a2_y, k_x, k_y));          // initialization of the conduction band energy
        e_minus = -std::abs(delta(a1_x, a1_y, a2_x, a2_y, k_x, k_y));    // initialization of the valence band energy

        double fd_plus;
        double fd_minus;

        fd_plus  = 1.0/(1.0 + std::exp(1./thermal_energy * e_plus));
        fd_minus = 1.0/(1.0 + std::exp(1./thermal_energy * e_minus));

        // initialization of the equilibrium reduced density matrix components for absent external field (@ initial time)
        rho11_0 = 1/2.0;

        rho22_0 = 1.0 - rho11_0;

        rho12_0 = 0.5 * (fd_minus - fd_plus) * delta(a1_x, a1_y, a2_x, a2_y, k_x, k_y)/(std::abs(delta(a1_x, a1_y, a2_x, a2_y, k_x, k_y)));

        rho21_0 = std::conj(rho12_0);

        // external gaussian & sinusoidal electric field parameters

        double E0_x, E0_y;                 // declaration of the reference electric field components
        double w, sigma, phi_0, t_center;   // declaration of the frequency, broadening, reference phase and center time
        double A0_x, A0_y;                  // declaration of the initial vector potential components
        double A_x, A_y;                    // declaration of the vector potential components
        E0_x = 1.0;                             // initialization of the reference x axis electric field component
        E0_y = 1.0;                             // initialization of the reference y axis electric field component
        w = 1.0;                                // initialization of the frequency of  the monochromatic and sinusoidal electromagnetic pulse
        sigma = 4.0;                            // initialization of the broadening of the gaussian electromagnetic pulse
        phi_0 = 0.0;                            // initialization of the reference phase of the monochromatic and sinusoidal electromagnetic pulse
        t_center = 20.0;                        // initialization of the center time variable of the gaussian pulse
        A0_x = 0.0;                             // initialization of the vector potential x axis component
        A0_y = 0.0;                             // initialization of the vector potential y axis component

        double k01, k02, k03, k04, k05, k06, k07;
        double k11, k12, k13, k14, k15, k16, k17;
        double error0, error1;
        double s0, s1;
        double time_step0, time_step1;

        double error2, error3, error4, error5;
        double s2, s3, s4, s5;
        double time_step2, time_step3, time_step4, time_step5;

        double opt_time_step, tolerance;

        std::vector <double> timestamp(N);
        Eigen::MatrixXd vector_potential(2, N);
        Eigen::MatrixXcd rdm(4, N);
        Eigen::MatrixXcd current_operator(8, N);
        Eigen::MatrixXcd current_vector(2, N);
        Eigen::MatrixXd brillouin_vector(1, 4);

        // 4th order Runge-Kutta integrator
        it = 0;
        opt_time_step = initial_time_step;
        max_time_step = 0.5;
        min_time_step = initial_time_step;
        A_x = A0_x;
        A_y = A0_y;
        rho11 = rho11_0; rho12 = rho12_0; rho21 = rho21_0; rho22 = rho22_0;
        t = initial_time;
        tolerance = 0.00001;

        int count;
        count = 0;

        std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

        while(t < final_time) {

                timestamp[it] = t;

                rdm(0, it) = rho11;
                rdm(1, it) = rho12;
                rdm(2, it) = rho21;
                rdm(3, it) = rho22;

                vector_potential(0,it) = A_x;
                vector_potential(1,it) = A_y;

                kt_x = k_x + vector_potential(0,it);
                kt_y = k_y + vector_potential(1,it);

                current_operator(0, it) = 0.0;
                current_operator(1, it) = 0.0;

                current_operator(2, it) = (1/3.0) * 1i * std::exp(1i * ((kt_x) * (a1_x + a2_x) + (kt_y) * (a1_y + a2_y))/3.) * (a1_x + a2_x + (-2.0 * a1_x + a2_x) * \
                                                                                                                                std::exp(-1i * ((kt_x) * (a1_x) + (kt_y) * (a1_y))/3.) + (a1_x - 2.0 * a2_x) * \
                                                                                                                                std::exp(-1i * ((kt_x) * (a2_x) + (kt_y) * (a2_y))/3.));

                current_operator(3, it) = (1/3.0) * 1i * std::exp(1i * ((kt_x) * (a1_x + a2_x) + (kt_y) * (a1_y + a2_y))/3.) * (a1_y + a2_y + (-2.0 * a1_y + a2_y) * \
                                                                                                                                std::exp(-1i * ((kt_x) * (a1_x) + (kt_y) * (a1_y))/3.) + (a1_y - 2.0 * a2_y) * \
                                                                                                                                std::exp(-1i * ((kt_x) * (a2_x) + (kt_y) * (a2_y))/3.));
                current_operator(4, it) = std::conj(current_operator(2, it));
                current_operator(5, it) = std::conj(current_operator(3, it));

                current_operator(6, it) = 0.0;
                current_operator(7, it) = 0.0;

                current_vector(0, it) = current_operator(0, it) * rho11 + current_operator(2, it) * rho21 + current_operator(4, it) * rho12 + current_operator(6, it) * rho22;
                current_vector(1, it) = current_operator(1, it) * rho11 + current_operator(3, it) * rho21 + current_operator(5, it) * rho12 + current_operator(7, it) * rho22;

                // electric field component integration

                k01 = opt_time_step * f_x(E0_x, sigma, w, phi_0, t, t_center);
                k02 = opt_time_step * f_x(E0_x, sigma, w, phi_0, t + opt_time_step * (1/5.), t_center);
                k03 = opt_time_step * f_x(E0_x, sigma, w, phi_0, t + opt_time_step * (3/10.), t_center);
                k04 = opt_time_step * f_x(E0_x, sigma, w, phi_0, t + opt_time_step * (4/5.), t_center);
                k05 = opt_time_step * f_x(E0_x, sigma, w, phi_0, t + opt_time_step * (8/9.), t_center);
                k06 = opt_time_step * f_x(E0_x, sigma, w, phi_0, t + opt_time_step * (1.0), t_center);
                k07 = opt_time_step * f_x(E0_x, sigma, w, phi_0, t + opt_time_step * (1.0), t_center);

                k11 = opt_time_step * f_y(E0_y, sigma, w, phi_0, t, t_center);
                k12 = opt_time_step * f_y(E0_y, sigma, w, phi_0, t + opt_time_step * (1./5.), t_center);
                k13 = opt_time_step * f_y(E0_y, sigma, w, phi_0, t + opt_time_step * (3./10.), t_center);
                k14 = opt_time_step * f_y(E0_y, sigma, w, phi_0, t + opt_time_step * (4./5.), t_center);
                k15 = opt_time_step * f_y(E0_y, sigma, w, phi_0, t + opt_time_step * (8./9.), t_center);
                k16 = opt_time_step * f_y(E0_y, sigma, w, phi_0, t + opt_time_step * (1.0), t_center);
                k17 = opt_time_step * f_y(E0_y, sigma, w, phi_0, t + opt_time_step * (1.0), t_center);

                A_x = A_x + k01 * (35./384.) + k03 * (500./1113.) + k04 * (125./192.) + k05 * (-2187./6784.) \
                      + k06 * (11./84.) + k07 * (0.0);
                A_y = A_y + k11 * (35./384.) + k13 * (500./1113.) + k14 * (125./192.) + k15 * (-2187./6784.) \
                      + k16 * (11./84.) + k17 * (0.0);

                error0 = std::abs(k01 * ((35./384.) - (5179./57600.)) + k03 * ((500./1113.) - (7571./16695.)) + k04 * ((125./192.) - (393./640.)) + k05 * ((-2187./6784.) - (-92097./339200.)) \
                                  + k06 * ((11./84.) - (187./2100.)) + k07 * ((0.0) - (1./40.)));
                error1 = std::abs(k11 * ((35./384.) - (5179./57600.)) + k13 * ((500./1113.) - (7571./16695.)) + k14 * ((125./192.) - (393./640.)) + k15 * ((-2187./6784.) - (-92097./339200.)) \
                                  + k16 * ((11./84.) - (187./2100.)) + k17 * ((0.0) - (1./40.)));

                s0 = std::pow(tolerance * opt_time_step/(2. * error0), (1./5.));
                s1 = std::pow(tolerance * opt_time_step/(2. * error1), (1./5.));

                time_step0 = s0 * opt_time_step;
                time_step1 = s1 * opt_time_step;

                ck01 = opt_time_step * f0(rho11, rho12, a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff);
                ck02 = opt_time_step * f0(rho11 + ck01 * (1./5.), rho12 + ck01 * (1./5.), a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff);
                ck03 = opt_time_step * f0(rho11 + ck01 * (3./40.) + ck02 * (9./40.), rho12 + ck01 * (3./40.) + ck02 * (9./40.), a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff);
                ck04 = opt_time_step * f0(rho11 + ck01 * (44./45.) + ck02 * (-56./15.) + ck03 * (32./9.), rho12 + ck01 * (44./45.) + ck02 * (-56./15.) + ck03 * (32./9.), a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff);
                ck05 = opt_time_step * f0(rho11 + ck01 * (19372./6561.) + ck02 * (-25360./2187.) + ck03 * (64448./6561.) + ck04 * (-212./729.), rho12 + ck01 * (19372./6561.) + ck02 * (-25360./2187.) + ck03 * (64448./6561.) \
                                          + ck04 * (-212./729.), a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff);
                ck06 = opt_time_step * f0(rho11 + ck01 * (9017./3168.) + ck02 * (-355./33.) + ck03 * (-46732./5247.) + ck04 * (49./176.) + ck05 * (-5103/18656.), rho12 + ck01 * (9017./3168.) + ck02 * (-355./33.) \
                                          + ck03 * (-46732./5247.) + ck04 * (49./176.) + ck05 * (-5103/18656.), a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff);
                ck07 = opt_time_step * f0(rho11 + ck01 * (35./384.) + ck03 * (500./1113.) + ck04 * (125./192.) + ck05 * (-2187/6784.) + ck06 * (11./84.), rho12 + ck01 * (35./384.) + ck03 * (500./1113.) \
                                          + ck04 * (125./192.) + ck05 * (-2187/6784.) + ck06 * (11./84.), a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff);

                e_plus   = std::abs(delta(a1_x, a1_y, a2_x, a2_y, kt_x, kt_y));
                e_minus  = -std::abs(delta(a1_x, a1_y, a2_x, a2_y, kt_x, kt_y));
                fd_plus  = 1.0/(1.0 + std::exp(1./(thermal_energy) * e_plus));
                fd_minus = 1.0/(1.0 + std::exp(1./(thermal_energy) * e_minus));

                ck11 = opt_time_step * f1(rho11, rho12, a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff, thermal_energy, fd_minus, fd_plus);
                ck12 = opt_time_step * f1(rho11 + ck11 * (1./5.), rho12 + ck11 * (1./5.), a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff, thermal_energy, fd_minus, fd_plus);
                ck13 = opt_time_step * f1(rho11 + ck11 * (3./40.) + ck12 * (9./40.), rho12 + ck11 * (3./40.) + ck12 * (9./40.), a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff, thermal_energy, fd_minus, fd_plus);
                ck14 = opt_time_step * f1(rho11 + ck11 * (44./45.) + ck12 * (-56./15.) + ck13 * (32./9.), rho12 + ck11 * (44./45.) + ck12 * (-56./15.) + ck13 * (32./9.), a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff, thermal_energy, fd_minus, fd_plus);
                ck15 = opt_time_step * f1(rho11 + ck11 * (19372./6561.) + ck12 * (-25360./2187.) + ck13 * (64448./6561.) + ck14 * (-212./729.), rho12 + ck11 * (19372./6561.) + ck12 * (-25360./2187.) + ck13 * (64448./6561.) \
                                          + ck14 * (-212./729.), a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff, thermal_energy, fd_minus, fd_plus);
                ck16 = opt_time_step * f1(rho11 + ck11 * (9017./3168.) + ck12 * (-355./33.) + ck13 * (-46732./5247.) + ck14 * (49./176.) + ck15 * (-5103/18656.), rho12 + ck11 * (9017./3168.) + ck12 * (-355./33.) \
                                          + ck13 * (-46732./5247.) + ck14 * (49./176.) + ck15 * (-5103/18656.), a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff, thermal_energy, fd_minus, fd_plus);
                ck17 = opt_time_step * f1(rho11 + ck11 * (35./384.) + ck13 * (500./1113.) + ck14 * (125./192.) + ck15 * (-2187/6784.) + ck16 * (11./84.), rho12 + ck11 * (35./384.) + ck13 * (500./1113.) \
                                          + ck14 * (125./192.) + ck15 * (-2187/6784.) + ck16 * (11./84.), a1_x, a1_y, a2_x, a2_y, kt_x, kt_y, damping_coeff, thermal_energy, fd_minus, fd_plus);

                rho11 = rho11 + ck01 * (35./384.) + ck03 * (500./1113.) + ck04 * (125./192.) + ck05 * (-2187./6784.) \
                        + ck06 * (11./84.) + ck07 * (0.0);
                rho12 = rho12 + ck11 * (35./384.) + ck13 * (500./1113.) + ck14 * (125./192.) + ck15 * (-2187./6784.) \
                        + ck16 * (11./84.) + ck17 * (0.0);
                rho21 = conj(rho12);
                rho22 = 1.0 - rho11;

                error2 = std::abs(std::real(ck01) * ((35./384.) - (5179./57600.)) + std::real(ck03)* ((500./1113.) - (7571./16695.)) + std::real(ck04) * ((125./192.) - (393./640.)) + std::real(ck05) \
                                  * ((-2187./6784.) - (-92097./339200.)) + std::real(ck06) * ((11./84.) - (187./2100.)) + std::real(ck07) * ((0.0) - (1./40.)));
                error3 = std::abs(std::imag(ck01) * ((35./384.) - (5179./57600.)) + std::imag(ck03)* ((500./1113.) - (7571./16695.)) + std::imag(ck04) * ((125./192.) - (393./640.)) + std::imag(ck05) \
                                  * ((-2187./6784.) - (-92097./339200.)) + std::imag(ck06) * ((11./84.) - (187./2100.)) + std::imag(ck07) * ((0.0) - (1./40.)));
                error4 = std::abs(std::real(ck11) * ((35./384.) - (5179./57600.)) + std::real(ck13)* ((500./1113.) - (7571./16695.)) + std::real(ck14) * ((125./192.) - (393./640.)) + std::real(ck15) \
                                  * ((-2187./6784.) - (-92097./339200.)) + std::real(ck16) * ((11./84.) - (187./2100.)) + std::real(ck17) * ((0.0) - (1./40.)));
                error5 = std::abs(std::imag(ck11) * ((35./384.) - (5179./57600.)) + std::imag(ck13)* ((500./1113.) - (7571./16695.)) + std::imag(ck14) * ((125./192.) - (393./640.)) + std::imag(ck15) \
                                  * ((-2187./6784.) - (-92097./339200.)) + std::imag(ck16) * ((11./84.) - (187./2100.)) + std::imag(ck17) * ((0.0) - (1./40.)));

                s2 = std::pow(tolerance * opt_time_step/(2. * error2), (1./5.));
                s3 = std::pow(tolerance * opt_time_step/(2. * error3), (1./5.));
                s4 = std::pow(tolerance * opt_time_step/(2. * error4), (1./5.));
                s5 = std::pow(tolerance * opt_time_step/(2. * error5), (1./5.));

                time_step2 = s2 * opt_time_step;
                time_step3 = s3 * opt_time_step;
                time_step4 = s4 * opt_time_step;
                time_step5 = s5 * opt_time_step;

                opt_time_step = std::min(time_step0, time_step1);
                opt_time_step = std::min(opt_time_step, time_step2);
                opt_time_step = std::min(opt_time_step, time_step3);
                opt_time_step = std::min(opt_time_step, time_step4);
                opt_time_step = std::min(opt_time_step, time_step5);

                if (opt_time_step < min_time_step) opt_time_step = min_time_step;
                else if(opt_time_step > max_time_step) opt_time_step = max_time_step;

                if(t + opt_time_step > final_time) opt_time_step = final_time - t;

                t = t + opt_time_step;
                it = it + 1;
                count = count + 1;

        }

        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

        // std::cout << (std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count())/(1000000.0);
        //std::cout << count;
        std::ofstream file;

        file.open("data.d");
        for (size_t i = 0; i < count; i++) {
                file << timestamp[i] << "\t" << real(rdm(0,i)) << "\t" << imag(rdm(0,i)) << "\t" << real(rdm(1, i)) << "\t" << imag(rdm(1, i)) << "\n";
        }

        return 0;


}
