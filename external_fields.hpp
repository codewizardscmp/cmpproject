#include <iostream>
#include <cmath>

#ifndef EXTERNAL_FIELDS_H
#define EXTERNAL_FIELDS_H

class external_fields
{

double frequency;
double init_phase;
double center_time;
double width;
double time;

public:
double gaussian_modulated_sinusoidal(double time,double width, double center_time, double frequency, double init_phase);

};

#endif
